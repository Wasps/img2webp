# coding: utf-8

'''
Vosa G Thomas @2020
Python script that convert images recursively in your CWD and following subdirectories to .webp format with a customizable input.
'''

from PIL import Image
import os
from os import walk

# Init variables
quality = 90
method = 6
# The image file extention to convert
image_extension = ".jpg", ".png", ".jpeg", ".bmp", ".gif"
# Whether to delete the original image or not (1 = yes, 0 = no)
replace = 1

cwd = os.getcwd()
LINEBREAK = "-------------------------------------------------------------- \n"

def get_folders():
    b = get_img_size(image_extension)
    print("Images sizes according to your settings: " + str(b) + " bytes")
    input()
    for subdir, dirs, files in os.walk(cwd):
        for filename in files:
            filepath = subdir + os.sep + filename
            if filepath.endswith(image_extension):
                print('converting :' + filepath)
                convert_to_webp(filepath, image_extension)
    print("done converting")
    print(LINEBREAK)
    a = get_img_size('.webp')
    print("Your images size after converting: " + str(a) + " bytes" + '\n')
    calculate_gain(b, a)
    input()


def convert_to_webp(filepath, image_extension):
    im = Image.open(filepath, 'r').convert("RGBA")
    im.save(
        os.path.splitext(filepath)[0] +
        '.webp',
        'webp',
        quality=quality,
        method=method)
    if replace:
        os.remove(filepath)


def get_img_size(ext):
    size = 0
    ext = ext
    for subdir, dirs, files in os.walk(cwd):
        for filename in files:
            filepath = subdir + os.sep + filename
            if filepath.endswith(ext):

                size = size + os.path.getsize(filepath)

    return size


def calculate_gain(before, after):
    b = before
    a = after
    print(LINEBREAK)
    try:
        print('Gain in size is ' + str(b - a) + ' or ' +
              str("{0:.1f}".format(((b - a) / b) * 100)) + '% size reduction')
    except BaseException:
        print("an error occurred, files didn't seem to be converted")
    print(LINEBREAK)


def menu():
    print("Welcome to the img to webp converter python script")
    print("This script will convert your images recursively in your sub-directory")
    print(
        "Here are your settings:\nImage extension to convert: " +
        str(image_extension) +
        "\nImage quality: " +
        str(quality) +
        "%" +
        "\nMethod (highest is best (max=6): " +
        str(method))
    if replace:
        print("I will permanently remove your original image")


menu()
get_folders()
