# Img2Webp Python

A quick and easy to use python script to convert images to the webp format.
Takes all specified images in a current working directory and sub-directory and convert them to webp using Pillow

Customizable:
- image quality
- images extensions (jpeg, bmp, jpg, png, etc)
- method (0-6) improve the file size but is slower (marginally)

Allow a file size reduction up to 80%